cmake_minimum_required(VERSION 3.2)
project(img_bucket)

add_compile_options(-std=c++11)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  tf
  tf2
  std_msgs
  sensor_msgs
  cv_bridge
  image_transport
  camera_info_manager
  message_generation
)

add_service_files(
  FILES
  ImageSequenceControl.srv
)

generate_messages(
  DEPENDENCIES
  std_msgs
)

find_package(OpenCV 3 REQUIRED)

catkin_package()

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
  ${OpenCV_INCLUDE_DIRS}
)

add_executable(image_seq_publisher src/image_sequence_publisher.cpp)
target_link_libraries(image_seq_publisher ${OpenCV_LIBS} ${catkin_LIBRARIES})


add_executable(detection_registerer src/detection_registerer.cpp)
target_link_libraries(detection_registerer ${OpenCV_LIBS} ${catkin_LIBRARIES})