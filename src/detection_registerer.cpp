#include <ros/ros.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf/transform_listener.h>

#include <boost/bind.hpp>

#include "img_bucket/ImageSequenceControl.h"

int counter_success = 0;
int counter_all = 0;

void chillitag_callback(const tf2_msgs::TFMessageConstPtr &pose_array, const std::string& tag_family, int tag_id,
                        const std::string& camera_frame_id, tf::TransformListener& tf_listener) {
  tf::StampedTransform transform;

  try {
    tf_listener.lookupTransform(std::string{"tag_"} + std::to_string(tag_id), camera_frame_id, ros::Time(0), transform);
    
    counter_success++;

    ROS_INFO("Tag Family: %s, Tag ID: %d is detected", tag_family.c_str(), tag_id);

  } catch (tf::TransformException& ex) {
    ROS_ERROR("Tag Family: %s, Tag ID: %d\n\t=> %s", tag_family.c_str(), tag_id, ex.what());
  }

  counter_all++;
}

int main(int argc, char *argv[]) {
  ros::init(argc, argv, "detection_registerer");

  ros::NodeHandle nh;
  ros::NodeHandle nh_("~");

  int tag_id;
  std::string tag_family;
  std::string camera_frame_id;

  nh_.param("tag_id", tag_id, 815);
  nh_.param("tag_family", tag_family, std::string{"chilitag"});
  nh_.param("camera_frame_id", camera_frame_id, std::string{"camera_link"});

  ROS_INFO("Detection registerer for Tag Family: %s, Tag ID: %d, Frame ID: %s", tag_family.c_str(), tag_id, camera_frame_id.c_str());

  tf::TransformListener tf_listener;

  auto chilitag_callback = boost::bind(&chillitag_callback, _1, boost::cref(tag_family), tag_id, boost::cref(camera_frame_id), boost::ref(tf_listener));

  auto chilli_tag_sub = nh.subscribe<tf2_msgs::TFMessage>("/tf", 1, chilitag_callback);

  ros::spin();

  ROS_INFO("Success: %d, All: %d", counter_success, counter_all);

  return 0;
}