#include <utility>
#include <string>
#include <vector>
#include <cassert>
#include <tuple>

#include <ros/ros.h>
#include <ros/package.h>

#include <boost/assign/list_of.hpp>
#include <boost/bind.hpp>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>
#include <camera_info_manager/camera_info_manager.h>

// default camera calibration
sensor_msgs::CameraInfo default_camera_info_from(const sensor_msgs::ImagePtr &img) {
  sensor_msgs::CameraInfo cam_info_msg;
  
  cam_info_msg.width = img->width;
  cam_info_msg.height = img->height;
  cam_info_msg.distortion_model = "plumb_bob";
  cam_info_msg.header.frame_id = img->header.frame_id;

  cam_info_msg.D.resize(5, 0.0);

  cam_info_msg.K = boost::assign::list_of(1.0)(0.0)(img->width / 2.0)
                                         (0.0)(1.0)(img->height / 2.0)
                                         (0.0)(0.0)(1.0);

  cam_info_msg.R = boost::assign::list_of(1.0)(0.0)(0.0)
                                         (0.0)(1.0)(0.0)
                                         (0.0)(0.0)(1.0);

  cam_info_msg.P = boost::assign::list_of(1.0)(0.0)(img->width / 2.0)(0.0)
                                         (0.0)(1.0)(img->height / 2.0)(0.0)
                                         (0.0)(0.0)(1.0)(0.0);
  
  return cam_info_msg;
}

auto load_images(const std::string& path, const std::string& tag_family, int tag_id, const std::string& extension) -> std::pair<std::vector<cv::Mat>, std::vector<cv::String>> {
    std::vector<cv::Mat> images;
    std::vector<cv::String> file_names;

    if (tag_family == "chilitag") {
      cv::glob(path + "/chi_" + std::to_string(tag_id) + "*." + extension, file_names, true);
    } else {
      cv::glob(path + "/aru_*." + extension, file_names, true);
    }

    for (const auto& img: file_names) {
      ROS_INFO("Loaded image: %s", img.c_str());

      images.emplace_back(cv::imread(img));
    }

    return {images, file_names};
}

int main(int argc, char *argv[]) {
  ros::init(argc, argv, "image_sequence_publisher");
  
  ros::NodeHandle nh;
  ros::NodeHandle nh_("~");

  int tag_id;
  bool continuous;
  float frequency;
  std::string tag_family;
  std::string images_path;
  std::string camera_frame_id;

  nh_.param("tag_id", tag_id, 815);
  nh_.param("path", images_path, ros::package::getPath("img_bucket") + "/dataset");
  nh_.param("camera_frame_id", camera_frame_id, std::string{"camera_link"});
  nh_.param("tag_family", tag_family, std::string{"aruco"});
  nh_.param("continuous", continuous, false);
  nh_.param("frequency", frequency, 1.0f);

  ROS_INFO("Tag Family: %s", tag_family.c_str());
  ROS_INFO("Tag ID: %d", tag_id);
  ROS_INFO("Publishing frequency: %.2f", frequency);
  ROS_INFO("Images dataset path: %s", images_path.data());
  ROS_INFO("Continuous mode is %s", continuous? "enabled" : "disabled");

  auto image_transport = image_transport::ImageTransport{nh};
  auto publisher = image_transport.advertiseCamera("image", 1);

  std::vector<cv::Mat> images;
  std::vector<cv::String> image_filenames;

  std::tie(images, image_filenames) = load_images(images_path, tag_family, tag_id, "png");

  assert(!images.empty());

  ROS_INFO("Loaded %lu images from %s", images.size(), images_path.c_str());

  camera_info_manager::CameraInfoManager manager(nh);

  ros::Rate rate(frequency);

  std_msgs::Header header;
  header.frame_id = camera_frame_id;

  int image_index = 0;

  while(nh.ok()) {

    if (publisher.getNumSubscribers() > 0) {

      auto image_id = image_index % images.size();

      ROS_INFO("Publishing image ID: %lu,\n\tURL: %s", image_id, image_filenames[image_id].c_str());

      if (!continuous && image_id == images.size() - 1) break;

      auto image_msg = cv_bridge::CvImage(header, sensor_msgs::image_encodings::BGR8, images[image_id]).toImageMsg();

      manager.setCameraInfo(default_camera_info_from(image_msg));  // images properties may vary

      auto camera_info_msg = manager.getCameraInfo();

      publisher.publish(*image_msg, camera_info_msg, ros::Time::now());

      image_index++;
    }

    ros::spinOnce();

    rate.sleep();
  }

  return 0;
}